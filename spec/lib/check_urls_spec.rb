require 'spec_helper'

describe "checks queries with ampersands" do
  f = File.open("spec/fixtures/routes.txt","r")
  f.each_line do |line|
    before(:each) do
      visit line
    end

    it "returns a 200" do
      expect(page.status_code).to be 200
    end
  end
end
