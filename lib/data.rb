require 'mongo'

module MongoData

  def self.db
    new_db = Mongo::MongoReplicaSetClient.new(ENV["MONGO_HOST"].split(","))
    @db ||= new_db[conf['mongo_db']]
  end

  def self.coll name
    self.db[name]
  end

  def self.conf
    @conf ||= JSON.parse(File.open('conf/app.json').read)
  end

end
